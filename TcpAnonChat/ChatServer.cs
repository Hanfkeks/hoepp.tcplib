﻿using Hoepp.TcpLib.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TcpAnonChat
{
    public class ChatServer
    {

        static void Main(string[] args)
        {
            new ChatServer();   //Create new Server. Server operates asynchronous, no reference needed
            Console.ReadKey();  //Making sure the app doesn't terminate
        }

        public int UserCount = 0;
        public NetworkHost Host;    //Server instance

        public ChatServer()
        {
            Host = new NetworkHost(7777);
            Host.ClientAdded += ConnectionEstablished;    //A client has been added and connection has been established
            Console.WriteLine("Server Starting up!");
            Host.Start();
        }

        private void ConnectionEstablished(RecievingClient client)
        {
            SetUsername(client);
            client.StringMessageRecieved += ClientMessageRecieved;    //A client has sent a message and it's being recieved by the server
            client.HybridMessageRecieved += Client_HybridMessageRecieved;
            client.SendStringMessage("> Hello "+client.Identifier+"!");
            client.Disconnected += Client_Disconnected; ;

            foreach (RecievingClient otherClient in Host.ActiveClients)  //Iterate to all connected Clients
            {
                if (client.Identifier == otherClient.Identifier) { continue; }  //Do not send the message to the client that just connected
                otherClient.SendStringMessage("> "+client.Identifier+" just joined");
            }

        }

        private void Client_HybridMessageRecieved(RecievingClient sender, string stringmessage, byte[] payload)
        {
            string senderName = sender.Identifier;  //Save the name
            string broadcastMessage = senderName + ": " + stringmessage;  //Extend message so that recievers know who wrote it
            foreach (RecievingClient client in Host.ActiveClients)  //Iterate to all connected Clients
            {
                if (client.Identifier == senderName) { continue; }  //Do not send the message to the client that just sent it
                client.SendHybridMessage(broadcastMessage,payload);
            }
        }

        private void Client_Disconnected(RecievingClient sender)
        {
            foreach (RecievingClient client in Host.ActiveClients)  //Iterate to all connected Clients
            {
                client.SendStringMessage("> " + sender.Identifier + " disconnected");
            }
        }

        private void ClientMessageRecieved(RecievingClient sender, string message)
        {
            string senderName = sender.Identifier;  //Save the name
            string broadcastMessage = senderName+": "+message;  //Extend message so that recievers know who wrote it
            foreach (RecievingClient client in Host.ActiveClients)  //Iterate to all connected Clients
            {
                if (client.Identifier == senderName) { continue; }  //Do not send the message to the client that just sent it
                client.SendStringMessage(broadcastMessage);
            }
        }

        public void SetUsername(RecievingClient client)
        {
            UserCount++;
            client.Identifier = "Anon " + UserCount;
        }

        

    }
}
