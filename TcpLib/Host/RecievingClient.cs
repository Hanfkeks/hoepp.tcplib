﻿using Hoepp.TcpLib;
using Hoepp.TcpLib.Client;
using Hoepp.TcpLib.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hoepp.TcpLib.Host
{
    /// <summary>
    /// Client Class that a server has in memory. Basically a reperesentation of a client from the point of view of the server
    /// </summary>
    public class RecievingClient
    {
        public const int MAX_SINGULAR_MESSAGE = NetworkClient.MAX_SINGULAR_MESSAGE;
        #region Delegates
        public delegate void MessageRecievedHandler(RecievingClient sender, byte[] rawmessage);
        public delegate void StringMessageRecievedHandler(RecievingClient sender, string stringmessage);
        public delegate void HybridMessageRecievedHandler(RecievingClient sender, string stringmessage, byte[] payload);
        public delegate void DisconnectedHandler(RecievingClient sender);
        public delegate void OnDisconnectHandler(RecievingClient sender);
        #endregion Delegates
        #region Events
        public event MessageRecievedHandler MessageRecieved;
        public event StringMessageRecievedHandler StringMessageRecieved;
        public event HybridMessageRecievedHandler HybridMessageRecieved;
        public event DisconnectedHandler Disconnected;
        public event OnDisconnectHandler OnDisconnect;
        #endregion Events

        #region Properties and Fields


        public string Identifier = "";
        public Socket Socket;
        public Thread SocketListenerThread;
        public volatile bool Active = true;
        public MessageManager MessageManager;
        #endregion Properties and Fields
        #region Constructors

        public RecievingClient(Socket socket)
        {
            Socket = socket;
            MessageManager = new MessageManager();
            SocketListenerThread = new Thread(SocketListening);
            SocketListenerThread.Start();
        }
        #endregion Constructors
        #region Methods

        public void SocketListening()
        {
            while (Active)
            {
                try
                {
                    byte[] rawdata = new byte[NetworkHost.DATA_SIZE + 1];
                    int size = Socket.Receive(rawdata);

                    if (size == 0)
                    {
                        Active = false;
                        FinishDisconnect();
                        return;
                    }

                    byte[] data = new byte[size];
                    Array.Copy(rawdata, data, size);


                    bool fullMessagesAvailable = MessageManager.Parse(data,0);

                    if (fullMessagesAvailable)
                    {
                        List<byte[]> newMessages = MessageManager.GetNewMessages();
                        foreach (byte[] rawMessage in newMessages)
                        {
                            MessageType type = MessageManager.GetMessageType(rawMessage);
                            MessageRecieved?.Invoke(this, rawMessage);
                            switch (type)
                            {
                                case MessageType.StringMessage:
                                    string decodedMessage = MessageManager.GetStringPayload(rawMessage);
                                    StringMessageRecieved?.BeginInvoke(this, decodedMessage, null, null);

                                    break;
                                case MessageType.HybridMessage:
                                    Tuple<string, byte[]> hybridPayload = MessageManager.GetHybridPayload(rawMessage);
                                    string hybridText = hybridPayload.Item1;
                                    byte[] hybridData = hybridPayload.Item2;
                                    HybridMessageRecieved?.BeginInvoke(this, hybridText, hybridData, null, null);
                                    break;
                                default:
                                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                                    Console.WriteLine(Identifier + ": Messages that aren't StringMessages are not supported yet");

                                    Console.ForegroundColor = ConsoleColor.Gray;
                                    break;
                            }
                        }
                    }
                    //Debug:
                    else
                    {
                        Console.WriteLine("Message not complete yet: " + Encoding.Unicode.GetString(data));
                    }
                    //End Debug



                }
                catch (Exception e)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connection Interrupted!");
                    Console.WriteLine("Interruption log: " + e.Message + "\nTrace:\n" + e.StackTrace);
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Active = false;
                }
                Thread.Sleep(1);//Avoid buisy wait
            }
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("Closing Connection of RecievingClient: " + Identifier);
            Console.ForegroundColor = ConsoleColor.Gray;
            OnDisconnect?.Invoke(this);
        }

        /// <summary>
        /// Only to be called once a disconnect has been applied in the Host object
        /// </summary>
        public void FinishDisconnect()
        {
            Disconnected?.Invoke(this);
        }

        public bool SendHybridMessage(string message, byte[] payload)
        {
            byte[] data = MessageManager.CreateHybridMessage(message, payload);
            return SendData(data);
        }

        public bool SendStringMessage(string message)
        {
            byte[] data = MessageManager.CreateStringMessage(message);
            return SendData(data);
        }

        public bool SendData(byte[] data)
        {
            lock (this)
            {
                try
                {
                    if (data.Length < MAX_SINGULAR_MESSAGE)
                    {
                        Socket.Send(data, 0, data.Length, SocketFlags.None);
                    }
                    else
                    {
                        int currentIndex = 0;
                        for (currentIndex = 0; currentIndex + MAX_SINGULAR_MESSAGE >= data.Length; currentIndex += MAX_SINGULAR_MESSAGE)
                        {
                            Socket.Send(data, currentIndex, MAX_SINGULAR_MESSAGE, SocketFlags.None);
                        }
                        Socket.Send(data, currentIndex, data.Length - currentIndex, SocketFlags.None);
                    }

                }
                catch (Exception)
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine("Data was not sent or lost! Client may miss out on some information");
                    return false;
                }
                return true;
            }
        }

        #endregion Methods

    }
}
