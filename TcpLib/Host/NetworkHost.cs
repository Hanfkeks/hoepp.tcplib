﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Hoepp.TcpLib.Host
{
    public class NetworkHost
    {
        public const int DATA_SIZE = 128 * 1024 * 1024;

        #region Delegates
        public delegate void ClientAddedEvent(RecievingClient client);
        #endregion Delegates
        #region Events
        public event ClientAddedEvent ClientAdded;
        #endregion Events

        public int Port;
        public volatile bool Exit = false;
        public List<RecievingClient> ActiveClients
        {
            get
            {
                lock (_activeClients)
                {
                    return _activeClients.ToList();
                }
            }
        }
        public List<RecievingClient> _activeClients = new List<RecievingClient>();
        public IRecievingClientFactory ClientFactory;
        public Thread ListenerThread = null;

        private TcpListener _listener;

        public NetworkHost(int port) : this(port, new DefaultRecievingClientFactory()) { }

        public NetworkHost(int port, IRecievingClientFactory clientFactory)
        {
            Port = port;
            ClientFactory = clientFactory;
            _listener = new TcpListener(IPAddress.Any, Port);
            ListenerThread = new Thread(ListenerLoop);
        }

        public void Start()
        {
            ListenerThread.Start();
        }



        public void ListenerLoop()
        {
            _listener.Start();
            while (!Exit)
            {
                Socket socket = _listener.AcceptSocket();
                RecievingClient rc = ClientFactory.CreateClient(socket);
                lock (_activeClients)
                {
                    _activeClients.Add(rc);
                }
                rc.OnDisconnect += ClientDisconnected;
                ClientAdded?.Invoke(rc);
            }
            foreach (RecievingClient rc in _activeClients)
            {
                rc.Active = false;
            }
            _listener.Stop();
        }

        private void ClientDisconnected(RecievingClient sender)
        {
            lock (_activeClients)
            {
                _activeClients.Remove(sender);
            }
            sender.FinishDisconnect();
        }

    }
}
