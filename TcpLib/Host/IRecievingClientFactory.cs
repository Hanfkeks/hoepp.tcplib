﻿using System.Net.Sockets;

namespace Hoepp.TcpLib.Host
{
    public interface IRecievingClientFactory
    {
        RecievingClient CreateClient(Socket socket);
    }

}