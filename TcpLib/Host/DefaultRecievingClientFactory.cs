﻿using Hoepp.TcpLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Hoepp.TcpLib.Host
{
    public class DefaultRecievingClientFactory : IRecievingClientFactory
    {
        public RecievingClient CreateClient(Socket socket)
        {
            return new RecievingClient(socket);
        }
    }
}
