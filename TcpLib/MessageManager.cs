﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hoepp.TcpLib
{
    public class MessageManager
    {
        public byte[] IncompleteMessage = new byte[0];
        public int ExpectedTotalLength = -1;

        public char SeperatorChar { get{return _seperatorChar;} set{_seperatorByte = null; _seperatorChar = value;} }
        public byte SeperatorByte
        {
            get
            {
                if(_seperatorByte.HasValue){return _seperatorByte.Value;}
                else{
                    byte[] bytes = Encoding.ASCII.GetBytes(new char[]{SeperatorChar});
                    _seperatorByte=bytes[0];
                    return _seperatorByte.Value;
                }
            }
        }
        private char _seperatorChar = '&';
        private byte? _seperatorByte = null;
        private List<byte[]> _fullMessages = new List<byte[]>();

        public List<byte[]> GetNewMessages() {
            
            lock (_fullMessages) {
                List<byte[]> rMessages = _fullMessages.ToList();
                _fullMessages.Clear();
                return rMessages;
            }
        }

        public bool Parse(byte[] input, int offset)
        {
            bool hasFound = false;
            int nextOffset = offset;
            bool multipleMessagesAvailable = false;
            lock(IncompleteMessage)
            {
                byte[] fullInput = new byte[input.Length-offset+IncompleteMessage.Length];
                Array.Copy(IncompleteMessage,0,fullInput,0,IncompleteMessage.Length);
                Array.Copy(input, offset, fullInput, IncompleteMessage.Length, input.Length-offset);

                int seperationIndex = GetSeperatorIndex(fullInput);
                
                if (seperationIndex > 0)
                {
                    int payloadLen = GetPayloadLength(fullInput);
                    int totalLen = seperationIndex + 1 + payloadLen;
                    if (totalLen <= fullInput.Length)
                    {
                        IncompleteMessage = new byte[0];
                        byte[] completeMessage = new byte[totalLen];
                        Array.Copy(fullInput, 0, completeMessage, 0, totalLen);
                        byte[] newInput = new byte[fullInput.Length - totalLen];
                        Array.Copy(fullInput, totalLen, newInput, 0, fullInput.Length - totalLen);
                        hasFound = true;
                        fullInput = newInput;
                        lock (_fullMessages)
                        {
                            _fullMessages.Add(completeMessage);
                        }
                        
                        if (totalLen < fullInput.Length)
                        {

                            Console.WriteLine("FOUND AT LEAST TWO MESSAGES IN ONE DATAPACKAGE! EXPECTED LENGTH: " + totalLen + ", ACTUAL LENGTH:" + fullInput.Length);
                            multipleMessagesAvailable = true;
                            nextOffset = totalLen + offset;
                            
                        }
                    }
                    else
                    {
                        hasFound = false;
                        IncompleteMessage = fullInput;
                    }
                }
                else
                {
                    hasFound = false;
                    IncompleteMessage = fullInput;
                }
                
                
            }
            if(multipleMessagesAvailable)
            { 
                Parse(input,nextOffset);
            }
            return hasFound;
        }

        public byte[] CreateStringMessage(string msg) {

            byte[] bytes = Encoding.Unicode.GetBytes(msg);
            return CreateMessage(bytes,MessageType.StringMessage);
        }

        public byte[] CreateMessage(byte[] msg, MessageType type) {
            byte typebyte = (byte)type;
            string prefixString = ""+msg.Length + SeperatorChar;
            byte[] prefix = Encoding.ASCII.GetBytes(prefixString);

            byte[] fullmessage = new byte[msg.Length + prefix.Length + 1];
            fullmessage[0] = typebyte;
            prefix.CopyTo(fullmessage, 1);
            msg.CopyTo(fullmessage, prefix.Length + 1);

            return fullmessage;
        }

        public byte[] CreateHybridMessage(string descriptor, byte[] payload)
        {
            
            byte[] stringpayload = Encoding.Unicode.GetBytes(descriptor);
            Int32 stringByteLength = stringpayload.Length;
            byte[] lengthAsBytes = BitConverter.GetBytes(stringByteLength);
            byte[] fullpayload = new byte[lengthAsBytes.Length + stringpayload.Length + payload.Length];
            System.Buffer.BlockCopy(lengthAsBytes, 0, fullpayload, 0, lengthAsBytes.Length);
            System.Buffer.BlockCopy(stringpayload, 0, fullpayload, lengthAsBytes.Length, stringpayload.Length);
            System.Buffer.BlockCopy(payload, 0, fullpayload, lengthAsBytes.Length + stringpayload.Length, payload.Length);

            return CreateMessage(fullpayload,MessageType.HybridMessage);
        }

        /// <summary>
        /// returns payload length
        /// if seperator could not be found, returns -1
        /// if prefix is invalid, returns -2
        /// </summary>
        /// <param name="fullmsg">full message byte array</param>
        /// <param name="fromPrefix">get payload length from prefix or from bytecount of payload</param>
        /// <returns></returns>
        public int GetPayloadLength(byte[] fullmsg, bool fromPrefix = true)
        {
            int currentByteIndex = GetSeperatorIndex(fullmsg);
            if(fromPrefix)
            {
                byte[] prefix = new byte[currentByteIndex-1];
                Array.Copy(fullmsg,1,prefix,0,currentByteIndex-1);
                string prefixString = Encoding.ASCII.GetString(prefix);
                try
                {
                    int len = int.Parse(prefixString);
                    return len;
                }
                catch(FormatException)
                {
                    return -2;
                }
            }else{
                return fullmsg.Length-currentByteIndex-1;
            }
        }

        public MessageType GetMessageType(byte[] fullmsg)
        {
            return (MessageType)fullmsg[0];
        }

        public int GetSeperatorIndex(byte[] fullmsg) {
            int currentByteIndex = 1;
            bool foundSeperator = false;
            while (currentByteIndex < fullmsg.Length)
            {
                if (fullmsg[currentByteIndex] == SeperatorByte)
                {
                    foundSeperator = true;
                    break;
                }
                currentByteIndex++;
            }
            if (!foundSeperator)
            {
                return -1;
            }
            return currentByteIndex;
        }

        public byte[] GetPayload(byte[] fullmsg)
        {
            int startIndex = GetSeperatorIndex(fullmsg) + 1;
            int size = fullmsg.Length - startIndex;
            byte[] payload = new byte[size];
                
            Array.Copy(fullmsg,startIndex,payload,0,size);
            return payload;
        }

        public string GetStringPayload(byte[] fullmsg)
        {
            byte[] payload = GetPayload(fullmsg);
            return Encoding.Unicode.GetString(payload);
        }

        public Tuple<string,byte[]> GetHybridPayload(byte[] fullmsg)
        {
            byte[] payload = GetPayload(fullmsg);
            byte[] stringlenPrefix = new byte[sizeof(Int32)];
            System.Buffer.BlockCopy(payload, 0, stringlenPrefix, 0, stringlenPrefix.Length);
            Int32 stringLength = BitConverter.ToInt32(stringlenPrefix,0);
            byte[] stringpayload = new byte[stringLength];
            byte[] wrappedPayload = new byte[payload.Length-stringLength-sizeof(Int32)];
            System.Buffer.BlockCopy(payload, stringlenPrefix.Length, stringpayload, 0, stringLength);
            System.Buffer.BlockCopy(payload, stringlenPrefix.Length+stringLength, wrappedPayload, 0, wrappedPayload.Length);
            return new Tuple<string, byte[]>(Encoding.Unicode.GetString(stringpayload), wrappedPayload);
        }

        /// <summary>
        /// Validates if the Message is plausible and complete
        /// </summary>
        /// <param name="fullmsg">byte message</param>
        /// <param name="length">length of message is set</param>
        /// <returns></returns>
        public bool ValidateMessage(byte[] fullmsg, out int length){
            int lenPrefix = GetPayloadLength(fullmsg,true);
            int lenPayload = GetPayloadLength(fullmsg,false);

            length = lenPrefix;

            if(lenPrefix>=0&&lenPayload>=0&&lenPrefix==lenPayload){
                return true;
            }else return false;

        }


    }
}
