﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Hoepp.TcpLib.Host;
using System.Net.Sockets;
using System.Net;

namespace Hoepp.TcpLib.Client
{

    public class NetworkClient
    {
        public const int RETRY_TIME = 1000;
        public const int MAX_SINGULAR_MESSAGE = 1024;

        #region Delegates
        public delegate void MessageRecievedHandler(byte[] rawmessage);
        public delegate void StringMessageRecievedHandler(string stringmessage);
        public delegate void HybridMessageRecievedHandler(string stringmessage, byte[] payload);
        public delegate void DisconnectedHandler();
        #endregion Delegates
        #region Events
        public event MessageRecievedHandler MessageRecieved;
        public event StringMessageRecievedHandler StringMessageRecieved;
        public event HybridMessageRecievedHandler HybridMessageRecieved;
        public event DisconnectedHandler Disconnected;
        #endregion Events

        public TcpClient Connection;
        public volatile bool Exit = false;
        public Thread ClientThread;
        public string Hostname;
        public MessageManager MessageManager;
        public int Port;

        private volatile bool _init = false;
        private NetworkStream Stream = null;

        public NetworkClient(string hostname, int port)
        {
            MessageManager = new MessageManager();
            Hostname = hostname;
            Port = port;
            ClientThread = new Thread(InitThread);
            
        }

        public NetworkClient(IPAddress address, int port) : this(address.ToString(), port)
        {

        }

        public void Start()
        {
            ClientThread.Start();
        }

        #region Methods
        public void ListenerLoop()
        {
            while (!Exit)
            {
                byte[] data = new byte[NetworkHost.DATA_SIZE+1];
                try
                {
                    int size = Connection.Client.Receive(data);
                    if (size == 0)
                    {
                        Exit = true;
                        Disconnected?.Invoke();
                        return;
                    }
                    byte[] messageData = new byte[size];
                    Array.Copy(data, messageData, size);
                    bool fullMessagesAvailable = MessageManager.Parse(messageData,0);
                    if (fullMessagesAvailable)
                    {
                        List<byte[]> newMessages = MessageManager.GetNewMessages();
                        foreach (byte[] rawMessage in newMessages)
                        {
                            MessageType type = MessageManager.GetMessageType(rawMessage);
                            MessageRecieved?.Invoke(rawMessage);
                            switch (type)
                            {
                                case MessageType.StringMessage:
                                    string decodedMessage = MessageManager.GetStringPayload(rawMessage);
                                    StringMessageRecieved?.Invoke(decodedMessage);
                                    break;
                                case MessageType.HybridMessage:
                                    Tuple<string, byte[]> hybridPayload = MessageManager.GetHybridPayload(rawMessage);
                                    string hybridText = hybridPayload.Item1;
                                    byte[] hybridData = hybridPayload.Item2;
                                    HybridMessageRecieved?.Invoke(hybridText, hybridData);
                                    break;
                                default:
                                    Console.ForegroundColor = ConsoleColor.DarkYellow;
                                    Console.WriteLine("(Client recieved non-string message)");
                                    Console.ForegroundColor = ConsoleColor.Gray;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("Message not complete yet: " + Encoding.Unicode.GetString(messageData));
                    }
                }
                catch
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connection got interrupted!");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    Thread.Sleep(RETRY_TIME);
                }
            }
        }

        public void InitThread()
        {
            while (!_init)
            {
                try
                {
                    Connection = new TcpClient(Hostname, Port);
                    _init = true;
                }
                catch (Exception)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Connection Failed, trying again");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    _init = false;
                    Thread.Sleep(RETRY_TIME);
                }
            }
            Connection.ReceiveBufferSize = NetworkHost.DATA_SIZE + 1;
            Connection.SendBufferSize = NetworkHost.DATA_SIZE + 1;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("Server reached!");
            Console.ForegroundColor = ConsoleColor.Gray;
            ListenerLoop();
        }

        public bool SendHybridMessage(string message, byte[] payload)
        {
            byte[] data = MessageManager.CreateHybridMessage(message, payload);
            return SendData(data);
        }

        public bool SendStringMessage(string message)
        {
            byte[] data = MessageManager.CreateStringMessage(message);
            return SendData(data);
        }

        public bool SendData(byte[] data)
        {
            lock (this)
            {
                if (Stream == null)
                {
                    Stream = Connection.GetStream();
                }
                bool success = false;
                try
                {

                    if (Stream.CanWrite)
                    {
                        if (data.Length < MAX_SINGULAR_MESSAGE)
                        {
                            Stream.Write(data, 0, data.Length);
                        }
                        else
                        {
                            int currentIndex = 0;
                            for (currentIndex = 0; currentIndex + MAX_SINGULAR_MESSAGE >= data.Length; currentIndex += MAX_SINGULAR_MESSAGE)
                            {
                                Stream.Write(data, currentIndex, MAX_SINGULAR_MESSAGE);
                            }
                            Stream.Write(data, currentIndex, data.Length - currentIndex);
                        }
                        success = true;
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Stream already closed");
                        Console.ForegroundColor = ConsoleColor.Gray;
                        try
                        {
                            Connection = new TcpClient(Hostname, Port);
                            Stream = null;
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("Stream successfully reset");
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }
                        catch (Exception)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("Stream cannot be reinstanciated. Check your connection!");
                            Console.ForegroundColor = ConsoleColor.Gray;
                        }
                    }

                }
                catch (Exception)
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error while sending command.");
                    Console.ForegroundColor = ConsoleColor.Gray;
                    success = false;
                }
                return success;
            }
        }

        #endregion Methods
    }
}
