﻿using Hoepp.TcpLib.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpAutomatedTester
{
    public class AutomatedTestClient
    {
        public const bool TestHybrid = true;
        public NetworkClient Connection;    //Tcp Client from Hoepp.TcpLib
        public int IterationCount = 100;

        static void Main(string[] args)
        {
            byte[] buffer = new byte[1024];
            var client = new AutomatedTestClient();
            while(true)
            { 
                Console.ReadLine();
                int recieveSize = client.Connection.Connection.Client.Receive(buffer);
                Console.WriteLine(recieveSize.ToString());
            }
            Console.WriteLine("Thread Ended");
        }

        public AutomatedTestClient()
        {
            Connection = new NetworkClient(System.Net.IPAddress.Parse("127.0.0.1"),7777);
            Connection.MessageRecieved += Connection_MessageRecieved;
            Connection.Start();
        }

        private void Connection_MessageRecieved(byte[] rawmessage)
        {
            string rstring = "";
            for (int i = 0; i < IterationCount; i++)
            {
                rstring += IterationCount.ToString();
            }
            IterationCount++;
            Connection.SendStringMessage(rstring);
            Console.WriteLine("Sending:");
            Console.WriteLine(rstring);
            Console.WriteLine();
        }
    }
}
