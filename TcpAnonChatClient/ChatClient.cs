﻿using Hoepp.TcpLib.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TcpAnonChat
{
    public class ChatClient
    {
        public const bool TestHybrid = true;

        public static void Main(string[] args)
        {
            ChatClient client = new ChatClient();
            while (true)
            {
                string msg = Console.ReadLine();    //Read input from console
                client.Send(msg);                   //Send what the user has written
            }
        }

        public NetworkClient Connection;    //Tcp Client from Hoepp.TcpLib

        public ChatClient()
        {
            Connection = new NetworkClient("127.0.0.1", 7777);//IP and Port. Setting to localhost so it communicates with a server on the same PC
            Connection.StringMessageRecieved += IncommingMessage;   //Subscribe StringMessageRecieved Event, call IncommingMessage method with message payload
            Connection.HybridMessageRecieved += HybridMessage;
            Connection.Start(); //Start Network Client and try to reach server at specified IP / Port
        }


        private void IncommingMessage(string stringmessage)
        {
            Console.ForegroundColor = ConsoleColor.Gray;    //Foreign messages should be gray
            Console.WriteLine(stringmessage);               //Write message to console
            Console.ForegroundColor = ConsoleColor.Green;   //own input should be green
        }

        private void HybridMessage(string stringmessage, byte[] payload)
        {
            Console.ForegroundColor = ConsoleColor.Gray;    //Foreign messages should be gray
            Console.WriteLine(stringmessage);               //Write message to console
            Console.ForegroundColor = ConsoleColor.Blue;    //Payload should be blue
            Console.WriteLine(Encoding.Unicode.GetString(payload));
            Console.ForegroundColor = ConsoleColor.Green;   //own input should be green
        }

        public void Send(string msg)
        {
            if (TestHybrid)
            {
                Connection.SendHybridMessage(msg, Encoding.Unicode.GetBytes(msg));
            }
            else
            {
                Connection.SendStringMessage(msg);              //TcpLib sends message to server
            }
        }

    }
}
