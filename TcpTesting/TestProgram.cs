﻿using Hoepp.TcpLib;
using Hoepp.TcpLib.Client;
using Hoepp.TcpLib.Host;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TcpTesting
{
    class TestProgram
    {
        public List<string> TestMessages = new List<string>();

        static void Main(string[] args)
        {
            MessageManager manager = new MessageManager();

            List<string> messages = new List<string>() { "A short string message","This is the second message. It's seperate from the first.","","The third message was empty, this one contains a & sign","This is message number 6"};

            SerializeSplitDeserializeTest(messages, manager);

            Console.WriteLine();
            Console.WriteLine("(De-)Serialization Test is finished, proceeding with propper Network test!");
            Console.WriteLine();

            new TestProgram(messages);

            Console.ReadKey();

        }

        public TestProgram(List<string> messages)
        {
            TestMessages = messages;

            IPAddress addr = IPAddress.Parse("127.0.0.1");
            int hostport = 7777;

            NetworkHost host = new NetworkHost(hostport);
            Console.WriteLine("Created Host");
            host.ClientAdded += Host_ClientAdded;

            NetworkClient client = new NetworkClient(addr,hostport);
            Console.WriteLine("Created Client");
            client.StringMessageRecieved += Client_StringMessageRecieved;

            host.Start();
            Thread.Sleep(100);
            client.Start();
            Thread.Sleep(500);
            foreach (string message in TestMessages)
            {
                client.SendStringMessage(message);
                Console.ForegroundColor = ConsoleColor.DarkBlue;
                Console.Write("Client Sent: ");
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.WriteLine(message);
                Thread.Sleep(100);
            }
        }

        private void Client_StringMessageRecieved(string stringmessage)
        {
            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write("Client Recieved: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(stringmessage);
        }

        private void Host_ClientAdded(RecievingClient client)
        {
            Console.WriteLine("Client Added");
            client.StringMessageRecieved += RecievingClient_MessageRecieved;
            //client.MessageRecieved += RecievingClient_AnyMessageRecieved;
        }

        private void RecievingClient_AnyMessageRecieved(RecievingClient sender, byte[] rawmessage)
        {
            DebugMessage(rawmessage,sender.MessageManager);
        }

        private void RecievingClient_MessageRecieved(RecievingClient sender, string content)
        {
            sender.SendStringMessage("Ack: \""+content+"\"");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("Server Recieved: ");
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(content);

        }

        public static void SerializeSplitDeserializeTest(List<string> messages, MessageManager manager)
        {
            List<byte[]> bytemessages = new List<byte[]>();
            foreach (string message in messages)
            {
                byte[] msg = manager.CreateStringMessage(message);
                Console.WriteLine("Message: " + message);
                bytemessages.Add(msg);
            }
            int totalsize = 0;
            foreach (var b in bytemessages)
            {
                totalsize += b.Length;
            }

            byte[] fullstream = new byte[totalsize];
            int currentindex = 0;
            foreach (var b in bytemessages)
            {
                Array.Copy(b, 0, fullstream, currentindex, b.Length);
                currentindex += b.Length;
            }

            List<int> cuts = new List<int>();
            Random rand = new Random((int)DateTime.Now.Ticks);
            currentindex = 0;

            List<byte[]> messageparts = new List<byte[]>();

            while (true)
            {
                int lastindex = currentindex;
                int partsize = rand.Next(10, 30);
                currentindex += partsize;
                if (currentindex < fullstream.Length)
                {
                    byte[] part = new byte[partsize];
                    Array.Copy(fullstream, lastindex, part, 0, currentindex - lastindex);
                    messageparts.Add(part);
                }
                else
                {
                    byte[] part = new byte[partsize];
                    Array.Copy(fullstream, lastindex, part, 0, fullstream.Length - lastindex);
                    messageparts.Add(part);
                    break;
                }
            }

            foreach (var b in messageparts)
            {
                bool hasMsg = manager.Parse(b);
                if (hasMsg)
                {
                    List<byte[]> msgs = manager.GetNewMessages();
                    foreach (byte[] b2 in msgs)
                    {
                        Console.WriteLine("Unpacked Message: " + manager.GetStringPayload(b2));
                    }
                }
            }
        }

        public void DebugMessage(byte[] msg, MessageManager manager) {
            string unpacked = manager.GetStringPayload(msg);
            Console.WriteLine();
            Console.WriteLine("Bytes:");
            foreach (byte b in msg)
            {
                if (b == 0) { Console.ForegroundColor = ConsoleColor.Red; }
                else if (b == manager.SeperatorByte) { Console.ForegroundColor = ConsoleColor.Green; }
                else { Console.ForegroundColor = ConsoleColor.Gray; }
                Console.Write("[" + b + "] ");
            }
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine();
            Console.WriteLine();
            Console.WriteLine("Chars:");
            foreach (byte b in msg)
            {
                Console.Write("[" + (char)b + "] ");
            }
            Console.WriteLine();
            Console.WriteLine();
            int len = 0;
            bool valid = manager.ValidateMessage(msg, out len);
            len = manager.GetPayloadLength(msg, false);
            Console.WriteLine("Valid: " + valid.ToString() + " Length: " + len + " Unpacked Message: " + unpacked);
        }
    }
}
